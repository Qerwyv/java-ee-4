<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:if test="${login== null}">
    <script>window.location.href='access-denied.jsp'</script>
</c:if>

<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8"%>
<html>
<head>
    <title>Add item</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link rel="stylesheet" href="resources/css/bootstrap.min.css">
</head>
<body>
<form action="AddItem">
    <div class="col-md-4 mb-3">
        <label for="validationServer01">Item title</label>
        <input autofocus type="text" class="form-control" name="item" id="validationServer01" placeholder="Item"
               value="" required>
    </div>
    <br>
    <div style="text-align: center;">
        <input type="submit" class="btn btn-outline-primary" value="Add" formmethod="post">
    </div>
</form>
</body>
</html>
