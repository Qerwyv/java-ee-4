<%--
  Created by IntelliJ IDEA.
  User: Arkenstone
  Date: 10.10.2019
  Time: 5:16
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

<head>
    <title>Registration</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="resources/css/bootstrap.min.css"/>
</head>
<body>
<div class="alert alert-success text-center" role="alert">
    <h2>Thank you for registration! We're send you an email to
        confirm your email address</h2>
</div>
<div class="text-center">
    <script type="text/javascript">
        var count = 6;

        function countDown() {
            var timer = document.getElementById("timer");
            if (count > 1) {
                count--;
                timer.innerHTML = "This page will redirect you to login page in <b>" + count + "</b> seconds.";
                setTimeout("countDown()", 1000);
            } else {
                window.location.href = "index.jsp"
            }
        }
    </script>
    <span id="timer">
<script type="text/javascript">countDown();</script>
</span>
</div>
</body>
</html>
