<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <c:if test="${error != null}">
        ${error = null}
    </c:if>
    <c:if test="${login == null}">
        <script>window.location.href = 'access-denied.jsp'</script>
    </c:if>
    <c:if test="${login != null}">
        <%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
        <jsp:useBean id="currentUser" scope="session" type="model.User"/>
        <jsp:useBean id="listOfFollowing" scope="session" type="java.util.ArrayList"/>
        <jsp:useBean id="numberOfFollowers" scope="session" type="java.lang.Integer"/>
        <jsp:useBean id="wishlist" scope="session" type="java.util.ArrayList"/>
        <jsp:useBean id="recent_activity" scope="session" type="java.util.ArrayList"/>
        <c:if test="${search != null}">
            <jsp:useBean id="searchList" scope="session" type="java.util.ArrayList"/>
            <jsp:useBean id="searchSequence" scope="session" type="java.lang.String"/>
        </c:if>
        <c:if test="${changesSuccessfullySaved != null}">
            <jsp:useBean id="changesSuccessfullySaved" scope="session" type="java.lang.Integer"/>
        </c:if>
    </c:if>
    <meta charset="UTF-8">
    <title>Title</title>

    <link rel="stylesheet" href="resources/css/bootstrap.css">
    <link rel="stylesheet" href="resources/css/bootstrap-grid.css">
    <link rel="stylesheet" href="resources/css/bootstrap-reboot.css">
    <link rel="stylesheet" href="resources/js/bootstrap.js">
    <link rel="stylesheet" href="resources/js/bootstrap.bundle.js">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <script src="resources/js/jquery-3.4.1.min.js"></script>
    <script src="resources/js/popper.min.js"></script>
    <script src="resources/js/bootstrap.js"></script>


</head>
<body>

<div class="container">
    <div class="row my-2">
        <div class="col-lg-8 order-lg-2">
            <ul class="nav nav-tabs">
                <li class="nav-item">
                    <a href="#profile" data-target="#profile" data-toggle="tab" class="nav-link active">Profile</a>
                </li>
                <li class="nav-item">
                    <a href="#following" data-target="#following" data-toggle="tab" class="nav-link">Following</a>
                </li>
                <li class="nav-item">
                    <a href="#wishlist" data-target="#wishlist" data-toggle="tab" class="nav-link">Wishlist</a>
                </li>
                <li class="nav-item">
                    <a href="#edit" data-target="#edit" data-toggle="tab" class="nav-link">Edit</a>
                </li>
                <li class="nav-item ml-auto" style="right: 0px">
                    <form method="post" action="LoginVerification">
                        <input type="submit" class="nav-link btn btn-outline-danger" name="logout" value="Log out"/>
                    </form>
                </li>
            </ul>

            <div class="tab-content py-4">

                <div class="tab-pane active" id="profile">
                    <c:if test="${not empty changesSuccessfullySaved}">
                        <div class="alert alert-info alert-dismissable">
                            <a class="panel-close close" data-dismiss="alert">×</a>
                            Changes successfully saved
                        </div>
                    </c:if>
                    <h5 class="mb-3">${currentUser.name} ${currentUser.surname}</h5> <!-- username -->
                    <div class="row">
                        <div class="col-md-6">
                            ${currentUser.phoneNumber}
                            <br>
                                ${currentUser.email}
                            <br>
                            <br>
                            <h6>About</h6>
                            <p>
                                <c:if test="${not empty currentUser.about}">
                                    ${currentUser.about}
                                </c:if>
                                <c:if test="${empty currentUser.about}">
                                    No additional details to show.
                                </c:if>
                            </p>
                            <h6>Hobbies</h6>
                            <p>
                                <c:if test="${not empty currentUser.hobbies}">
                                    ${currentUser.hobbies}
                                </c:if>
                                <c:if test="${empty currentUser.hobbies}">
                                    No additional details to show.
                                </c:if>
                            </p>
                        </div>
                        <div class="col-md-6">
                            <%--                            <h6>Recent badges</h6>--%>
                            <%--                            <a href="#" class="badge badge-dark badge-pill">html5</a>--%>
                            <%--                            <a href="#" class="badge badge-dark badge-pill">react</a>--%>
                            <%--                            <a href="#" class="badge badge-dark badge-pill">codeply</a>--%>
                            <%--                            <a href="#" class="badge badge-dark badge-pill">angularjs</a>--%>
                            <%--                            <a href="#" class="badge badge-dark badge-pill">css3</a>--%>
                            <%--                            <a href="#" class="badge badge-dark badge-pill">jquery</a>--%>
                            <%--                            <a href="#" class="badge badge-dark badge-pill">bootstrap</a>--%>
                            <%--                            <a href="#" class="badge badge-dark badge-pill">responsive-design</a>--%>
                            <%--                            <hr>--%>
                            <span class="badge badge-primary"><i class="fa fa-user"></i> ${numberOfFollowers} Followers</span>
                            <span class="badge badge-danger"><i class="fa fa-eye"></i> ${fn:length(listOfFollowing)} Following</span>
                            <span class="badge badge-success"><i class="fa fa-cog"></i> ${fn:length(wishlist)} Items in wishlist</span>

                        </div>
                        <div class="col-md-12">
                            <h5 class="mt-2"><span class="fa fa-clock-o ion-clock float-right"></span> Recent Activity
                            </h5>
                            <table class="table table-sm table-hover table-striped">
                                <thead>
                                <tr>
                                    <th scope="col"></th>
                                    <th scope="col"></th>
                                </tr>
                                </thead>
                                <tbody>
                                <c:forEach items="${recent_activity}" var="activity">
                                    <tr>
                                        <td>
                                            You <strong><c:out value="${activity.text}"> </c:out></strong>
                                        </td>
                                        <td>
                                            <c:out value="${activity.datetime}"></c:out>
                                        </td>
                                    </tr>
                                </c:forEach>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!--/row-->
                </div>

                <div class="tab-pane" id="following">
                    <c:if test="${search != null}">
                        <div style="text-align: center;">
                            <form action="profile#following" method="get">
                                <input type="submit" class="btn btn-success" value="Get back to following list"/>
                            </form>
                        </div>
                    </c:if>
                    <div class="alert alert-info alert-dismissable">
                        <a class="panel-close close" data-dismiss="alert">×</a>
                        <strong>Notice:</strong> users can't see the list of their followers
                    </div>
                    <form action="profile#following" method="get">
                        <div class="input-group mb-3">
                            <input type="text" name="searchUserField" class="form-control" id="searchUserField"
                                   placeholder="User email/phone number/full name" aria-label="Recipient's username"
                                   aria-describedby="basic-addon2">
                            <div class="input-group-append">
                                <button class="btn btn-outline-secondary" type="submit">Search</button>
                            </div>
                        </div>
                    </form>
                    <c:if test="${search == null}">
                        <c:if test="${fn:length(listOfFollowing) == 0}">
                            <div class="alert alert-info alert-dismissable">
                                <a class="panel-close close" data-dismiss="alert">×</a>
                                You haven't followed anyone yet :( <br>
                                Try to find your friends using search form above
                            </div>
                        </c:if>
                        <c:if test="${fn:length(listOfFollowing) != 0}">
                            <table class="table table-striped">
                                <thead>
                                <tr class="table-success">
                                    <th scope="col">Name</th>
                                    <th scope="col">Surname</th>
                                    <th scope="col">Phone number</th>
                                    <th scope="col">Email</th>
                                    <th scope="col">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <c:forEach items="${listOfFollowing}" var="item">
                                    <tr>
                                        <td>
                                            <c:out value="${item.name}"></c:out>
                                        </td> <!-- ID -->
                                        <td>
                                            <c:out value="${item.surname}"></c:out>
                                        </td>
                                        <td>
                                            <c:out value="${item.phoneNumber}"></c:out>
                                        </td>
                                        <td>
                                            <c:out value="${item.email}"></c:out>
                                        </td>
                                        <td>
                                            <c:url value="profile" var="C_OpenProfile">
                                                <c:param name="email" value="${item.email}"/>
                                            </c:url>
                                            <a class="btn btn-outline-info btn-sm" href="${C_OpenProfile}"
                                               role="button">
                                                Open profile</a>

                                            <c:url value="FollowUnfollow" var="Unfollow">
                                                <c:param name="emailUnfollow" value="${item.email}"/>
                                            </c:url>
                                            <a class="btn btn-outline-danger btn-sm"
                                               onclick="return confirm('Are you sure?')"
                                               role="button" href="${Unfollow}">Unfollow</a>
                                        </td>
                                    </tr>
                                </c:forEach>
                                </tbody>
                            </table>
                        </c:if>
                    </c:if>
                    <c:if test="${search != null}">
                        <div class="alert alert-info alert-dismissable">
                            <a class="panel-close close" data-dismiss="alert">×</a>
                            There are ${fn:length(searchList)} results that match your search
                        </div>
                        <script>
                            document.getElementById('searchUserField').value = '${searchSequence}';
                        </script>
                        <c:if test="${fn:length(searchList) == 0}">
                        </c:if>
                        <c:if test="${fn:length(searchList) != 0}">
                            <table class="table table-striped">
                                <thead>
                                <tr class="table-success">
                                    <th scope="col">Name</th>
                                    <th scope="col">Surname</th>
                                    <th scope="col">Phone number</th>
                                    <th scope="col">Email</th>
                                    <th scope="col">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <c:forEach items="${searchList}" var="item">
                                    <tr>
                                        <td>
                                            <c:out value="${item.name}"></c:out>
                                        </td> <!-- ID -->
                                        <td>
                                            <c:out value="${item.surname}"></c:out>
                                        </td>
                                        <td>
                                            <c:out value="${item.phoneNumber}"></c:out>
                                        </td>
                                        <td>
                                            <c:out value="${item.email}"></c:out>
                                        </td>
                                        <td>
                                            <c:url value="profile" var="C_OpenProfile">
                                                <c:param name="email" value="${item.email}"/>
                                            </c:url>
                                            <a class="btn btn-outline-info btn-sm" href="${C_OpenProfile}"
                                               role="button">
                                                Open profile</a>

                                                <%--                                            <c:url value="FollowUnfollow" var="Follow">--%>
                                                <%--                                                <c:param name="emailFollow" value="${item.email}"/>--%>
                                                <%--                                            </c:url>--%>
                                                <%--                                            <a class="btn btn-outline-success btn-sm"--%>
                                                <%--                                               onclick="return confirm('Are you sure?')"--%>
                                                <%--                                               role="button" href="${Follow}">Follow</a>--%>
                                            <c:set var="contains" value="false"/>
                                            <c:forEach var="followingUser" items="${listOfFollowing}">
                                                <c:if test="${item.email eq followingUser.email}">
                                                    <c:set var="contains" value="true"/>
                                                    <!-- this user is already followed by current user
                                                    so change "follow' to 'unfollow' -->
                                                    <c:url value="FollowUnfollow" var="Unfollow">
                                                        <c:param name="emailUnfollow" value="${item.email}"/>
                                                    </c:url>
                                                    <a class="btn btn-outline-danger btn-sm"
                                                       onclick="return confirm('Are you sure?')"
                                                       role="button" href="${Unfollow}">Unfollow</a>
                                                </c:if>
                                            </c:forEach>
                                            <c:if test="${contains eq false}">
                                                <c:url value="FollowUnfollow" var="Follow">
                                                    <c:param name="emailFollow" value="${item.email}"/>
                                                </c:url>
                                                <a class="btn btn-outline-success btn-sm"
                                                   onclick="return confirm('Are you sure?')"
                                                   role="button" href="${Follow}">Follow</a>
                                            </c:if>
                                        </td>
                                    </tr>
                                </c:forEach>
                                </tbody>
                            </table>
                        </c:if>
                    </c:if>

                </div>


                <div class="tab-pane" id="wishlist">
                    <%--                    <div class="alert alert-info alert-dismissable">--%>
                    <%--                        <a class="panel-close close" data-dismiss="alert">×</a> This is an <strong>.alert</strong>. Use--%>
                    <%--                        this to show important messages to the user.--%>
                    <%--                    </div>--%>
                    <c:if test="${fn:length(wishlist) == 0}">
                        <div class="alert alert-info alert-dismissable">
                            <a class="panel-close close" data-dismiss="alert">×</a>
                            You don't have any items in wishlist :( <br>
                            Try to add some using button below
                        </div>
                    </c:if>
                    <c:if test="${fn:length(wishlist) != 0}">
                        <table class="table table-striped">
                            <thead>
                            <tr class="table-success">
                                <th scope="col">Id</th>
                                <th scope="col">Item</th>
                                <th scope="col">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <c:forEach items="${wishlist}" var="item">
                                <tr>
                                    <th scope="row">
                                        <c:out value="${item.item_id}"></c:out>
                                    </th> <!-- ID -->
                                    <td>
                                        <c:out value="${item.item}"></c:out>
                                    </td>
                                    <td>
                                        <c:url value="EditItem" var="C_EditItem">
                                            <c:param name="itemForEditId" value="${item.item_id}"/>
                                        </c:url>
                                        <a class="btn btn-outline-info btn-sm" href="${C_EditItem}"
                                           role="button">Edit</a>

                                        <c:url value="DeleteItem" var="C_DeleteItem">
                                            <c:param name="id" value="${item.item_id}"/>
                                        </c:url>
                                        <a class="btn btn-outline-danger btn-sm"
                                           onclick="return confirm('Are you sure?')"
                                           role="button" href="${C_DeleteItem}">Delete</a>
                                    </td>
                                </tr>
                            </c:forEach>
                            </tbody>
                        </table>
                    </c:if>
                    <div style="text-align: center;">
                        <form action="AddItem" method="post">
                            <input type="submit" class="btn btn-success" value="Add item"/>
                        </form>
                    </div>
                </div>

                <div class="tab-pane" id="edit">
                    <form role="form" autocomplete="off" action="profile" method="post">
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label form-control-label">First name</label>
                            <div class="col-lg-9">
                                <input class="form-control" type="text" name="name" value="${currentUser.name}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label form-control-label">Last name</label>
                            <div class="col-lg-9">
                                <input class="form-control" type="text" name="surname" value="${currentUser.surname}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label form-control-label">Email</label>
                            <div class="col-lg-9">
                                <c:if test="${emailExists == null}">
                                    <input type="email" class="form-control" name="email"
                                           id="validationServer04"
                                           oninvalid="this.setCustomValidity('Your input does not match field requirements')"
                                           oninput="this.setCustomValidity('')"
                                           placeholder="Email address (e. g. sample@mail.com)"
                                           value="${currentUser.email}" required>
                                </c:if>
                                <c:if test="${emailExists != null}">
                                    <input autofocus type="email" class="form-control is-invalid" name="email"
                                           id="validationServer04"
                                           placeholder="Email address (e. g. sample@mail.com)" value="${emailExists}"
                                           required>
                                    <div class="invalid-feedback">
                                        This email address is already registered
                                    </div>
                                </c:if>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label form-control-label">Phone number</label>
                            <div class="col-lg-9">
                                <c:if test="${phoneNumberExists == null}">
                                    <input type="text" class="form-control" name="phoneNumber" pattern="[+][0-9]{7,15}"
                                           id="validationServer02"
                                           oninvalid="this.setCustomValidity('Your input does not match field requirements')"
                                           oninput="this.setCustomValidity('')"
                                           placeholder="Phone number (e.g. +380951234567)"
                                           value="${currentUser.phoneNumber}" required>
                                </c:if>
                                <c:if test="${phoneNumberExists != null}">
                                    <input autofocus type="text" class="form-control is-invalid" name="phoneNumber"
                                           pattern="[+][0-9]{7,15}"
                                           id="validationServer02"
                                           placeholder="Phone number (e.g. +380951234567)" value="${phoneNumberExists}"
                                           required> <!--incorrect email -->
                                    <div class="invalid-feedback">
                                        This phone number is already registered
                                    </div>
                                </c:if>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label form-control-label">About</label>
                            <div class="col-lg-9">
                                <input class="form-control" type="text" name="about" value="${currentUser.about}"
                                       placeholder="Describe yourself in a few words">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label form-control-label">Hobbies</label>
                            <div class="col-lg-9">
                                <input class="form-control" type="text" name="hobbies" value="${currentUser.hobbies}"
                                       placeholder="What do you do in your spare time?">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label form-control-label">Password</label>
                            <div class="col-lg-9">
                                <input class="form-control" type="password" value="" minlength="8" name="password"
                                       id="password"
                                       oninvalid="this.setCustomValidity('Your input does not match field requirements. ' +
                                        'Password must be at least 8 characters long')"
                                       oninput="this.setCustomValidity('')"
                                       onkeyup="checkPasswordMatching();">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label form-control-label">Confirm password</label>
                            <div class="col-lg-9">
                                <input class="form-control" type="password" value="" id="password_confirmation"
                                       onkeyup="checkPasswordMatching();">
                                <span id="message_about_password"></span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-lg-3 col-form-label form-control-label"></label>
                            <div class="col-lg-9">
                                <input type="reset" class="btn btn-secondary" value="Cancel">
                                <input type="submit" class="btn btn-primary" value="Save Changes" id="submit_button">
                            </div>
                        </div>
                    </form>
                </div>
                <script type="text/javascript">
                    var checkPasswordMatching = function () {
                        var password = document.getElementById('password').value;
                        var password_confirmation = document.getElementById('password_confirmation').value;
                        var message = document.getElementById('message_about_password');
                        var submit_button = document.getElementById('submit_button');
                        if (password == password_confirmation) {
                            message.style.color = 'green';
                            message.innerHTML = 'matching';
                            submit_button.disabled = false;
                        } else {
                            message.style.color = 'red';
                            message.innerHTML = 'not matching';
                            submit_button.disabled = true;
                        }
                        if ((password == null && password_confirmation == null) || (password == "" && password_confirmation == "")) {
                            message.innerHTML = '';
                        }
                    }
                </script>
                <script>
                    var hash = window.location.hash;
                    hash && $('ul.nav a[href="' + hash + '"]').tab('show');
                    $('.nav-tabs a').click(function (e) {
                        $(this).tab('show');
                        var scrollmem = $('body').scrollTop();
                        window.location.hash = this.hash;
                        $('html,body').scrollTop(scrollmem);
                    });
                </script>
            </div>
        </div>
        <div class="col-lg-4 order-lg-1 text-center">
            <img src="https://cdn3.iconfinder.com/data/icons/flat-design-hands-icons/128/31-256.png"
                 class="mx-auto img-fluid img-circle d-block" alt="avatar">
        </div>

    </div>
</div>
</body>
<script>
    $(document).ready(function () {
        $(".nav-tabs a").click(function () {
            $(this).tab('show');
        });
    });
</script>
</html>