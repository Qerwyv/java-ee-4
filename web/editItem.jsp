<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<c:if test="${login== null}">
    <script>window.location.href = 'access-denied.jsp'</script>
</c:if>

<c:if test="${login != null}">
    <jsp:useBean id="currentItem" scope="session" type="model.Item"/>
</c:if>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Edit item</title>
    <link rel="stylesheet" href="resources/css/bootstrap.min.css">
</head>
<body>
<form action="EditItem">
        <div class="col-md-4 mb-3" content="center">
            <label for="validationServer01">Item title</label>
            <input autofocus type="text" class="form-control" name="item" id="validationServer01" placeholder="Item"
                   value="${currentItem.item}" required>
            <br>
            <input type="submit" class="btn btn-outline-primary" value="Edit">
        </div>
</form>
</body>
</html>
