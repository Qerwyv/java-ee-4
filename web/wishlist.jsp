<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
    <title>List</title>
    <link rel="stylesheet" href="resources/css/bootstrap.min.css">
    <script src="resources/js/jquery-3.4.1.min.js"></script>
    <c:if test="${error != null}">
        ${error = null}
    </c:if>
    <c:if test="${login == null}">
        <script>window.location.href = 'access-denied.jsp'</script>
    </c:if>
    <c:if test="${login != null}">
        <%@ page contentType="text/html;charset=UTF-8" language="java" %>
        <c:if test="${wishlist != null}">
            <jsp:useBean id="wishlist" scope="session" type="java.util.ArrayList"/>
        </c:if>
        <c:if test="${otherWishlistAdmin != null}">
            <jsp:useBean id="otherWishlistAdmin" scope="session" type="java.util.ArrayList"/>
        </c:if>
        <jsp:useBean id="currentUser" scope="session" type="model.User"/>
    </c:if>

</head>
<body>
<div style="text-align: center;">
    <nav class="navbar sticky-top navbar-light bg-light" style="background-color: #e3f2fd; height: 50px" size="auto">
        <form class="form-inline">
            <a class="btn btn-outline-success btn-sm" href="<c:url value="/wishlist"/>">Refresh list</a>
        </form>
        <form class="form-inline">
            <a class="btn btn-outline-success btn-sm" href="<c:url value="/List"/>">Back to users list</a>
        </form>
        <form class="form-inline">
            <a class="navbar-brand" style="font-size: 15px"><b><c:out value="${currentUser.name}"> </c:out> <c:out
                    value="${currentUser.surname}"> </c:out>'s wishlist</b></a>
        </form>
    </nav>
    <table class="table table-striped">
        <thead>
        <tr class="table-success">
            <th scope="col">Id</th>
            <th scope="col">Item</th>
            <th scope="col">Action</th>
        </tr>
        </thead>
        <tbody>
        <c:if test="${otherWishlistAdmin != null}">
            <c:forEach items="${otherWishlistAdmin}" var="item">
                <tr>
                    <th scope="row">
                        <c:out value="${item.item_id}"></c:out>
                    </th> <!-- ID -->
                    <td>
                        <c:out value="${item.item}"></c:out>
                    </td>
                    <td>
                        <c:url value="EditItem" var="C_EditItem">
                            <c:param name="itemForEditId" value="${item.item_id}"/>
                        </c:url>
                        <a class="btn btn-outline-info btn-sm" href="${C_EditItem}" role="button">Edit</a>

                        <c:url value="DeleteItem" var="C_DeleteItem">
                            <c:param name="id" value="${item.item_id}"/>
                        </c:url>
                        <a class="btn btn-outline-danger btn-sm" onclick="return confirm('Are you sure?')"
                           role="button" href="${C_DeleteItem}">Delete</a>
                    </td>
                </tr>
            </c:forEach>
        </c:if>
        <c:if test="${otherWishlistAdmin == null}">
            <c:forEach items="${wishlist}" var="item">
                <tr>
                    <th scope="row">
                        <c:out value="${item.item_id}"></c:out>
                    </th> <!-- ID -->
                    <td>
                        <c:out value="${item.item}"></c:out>
                    </td>
                    <td>
                        <c:url value="EditItem" var="C_EditItem">
                            <c:param name="itemForEditId" value="${item.item_id}"/>
                        </c:url>
                        <a class="btn btn-outline-info btn-sm" href="${C_EditItem}" role="button">Edit</a>

                        <c:url value="DeleteItem" var="C_DeleteItem">
                            <c:param name="id" value="${item.item_id}"/>
                        </c:url>
                        <a class="btn btn-outline-danger btn-sm" onclick="return confirm('Are you sure?')"
                           role="button" href="${C_DeleteItem}">Delete</a>
                    </td>
                </tr>
            </c:forEach>
        </c:if>
        </tbody>
    </table>
    <form action="AddItem" method="post">
        <input type="submit" class="btn btn-success" value="Add item"/>
    </form>
</div>
</body>
</html>
