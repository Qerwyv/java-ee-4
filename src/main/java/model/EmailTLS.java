package model;

import com.sun.mail.util.MailConnectException;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;
import java.util.UUID;

public class EmailTLS {

    public static void sendConfirmationLink(String recipientEmail, String UUID) {
        final String username = "gifter.noreply@gmail.com";
        final String password = "gifter_coursework";

        Properties prop = new Properties();
        prop.put("mail.smtp.host", "smtp.gmail.com");
        prop.put("mail.smtp.port", "587");
        prop.put("mail.smtp.auth", "true");
        prop.put("mail.smtp.starttls.enable", "true"); //TLS

        Session session = Session.getInstance(prop,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });

        try {

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("Gifter"));
            message.setRecipients(
                    Message.RecipientType.TO,
                    InternetAddress.parse(recipientEmail)
            );
            message.setSubject("Welcome to Gifter");
            message.setContent("Thanks for making an account at Gifter.<br>" +
                    "To confirm your email, " +
                    "please follow this <a href=\"http://localhost:8081/registration?uuid="+ UUID +"\">link</a>.<br>" +
                    "If you did not request a registration in Gifter, please <b>ignore</b> this e-mail.", "text/html");
            Transport.send(message);
            System.out.println("Email (confirmation of registration) send to " + recipientEmail);

        } catch (MailConnectException mailConnectException) {
            System.out.println("Please connect to the Internet. " +
                    "Google host " + mailConnectException.getHost() + " cannot be found!");
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    public static void sendLinkToResetPassword (String recipientEmail, String UUID) {
        final String username = "gifter.noreply@gmail.com";
        final String password = "gifter_coursework";

        Properties prop = new Properties();
        prop.put("mail.smtp.host", "smtp.gmail.com");
        prop.put("mail.smtp.port", "587");
        prop.put("mail.smtp.auth", "true");
        prop.put("mail.smtp.starttls.enable", "true"); //TLS

        Session session = Session.getInstance(prop,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });

        try {

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("Gifter"));
            message.setRecipients(
                    Message.RecipientType.TO,
                    InternetAddress.parse(recipientEmail)
            );
            message.setSubject("Password reset at Gifter");
            message.setContent("To reset your current password " +
                    "please follow this <a href=\"http://localhost:8081/ResetPassword?uuid="+ UUID +"\">link</a>.<br>" +
                    "If you did not request a password reset at Gifter, please <b>ignore</b> this e-mail.", "text/html");
            Transport.send(message);

            System.out.println("Email (password reset) send to " + recipientEmail);

        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

}