package model;

import com.sun.scenario.effect.impl.sw.sse.SSEBlend_SRC_OUTPeer;

public class Item {
    private Integer user_id;
    private Integer item_id;
    private String item;

    public Item(Integer user_id, Integer item_id, String item) {
        this.user_id = user_id;
        this.item_id = item_id;
        this.item = item;
    }

    public Item(Integer item_id, String item) {
        this.item_id = item_id;
        this.item = item;
    }


    public Integer getUser_id() {
        return user_id;
    }

    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    public Integer getItem_id() {
        return item_id;
    }

    public void setItem_id(Integer item_id) {
        this.item_id = item_id;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }
}
