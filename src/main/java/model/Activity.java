package model;

public class Activity {
    private String text;
    private String datetime;

    public Activity (String text, String datetime) {
        this.text = text;
        this.datetime = datetime;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }
}
