package servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static servlet.Globals.*;

@WebServlet("/Redirect")
public class RedirectServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getParameterMap().isEmpty()) {
            System.out.println("empty");
            resp.sendRedirect(REDIRECT_TO_LOGIN_PAGE);
            return;
        }
        if (req.getParameterMap().containsKey("register_new_admin")) {
            req.setAttribute("admin", "admin");
            req.getRequestDispatcher("registration.jsp").forward(req, resp);
        }
    }
}
