package servlet;

import database.Database;
import model.Item;
import model.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@WebServlet("/DeleteItem")
public class DeleteItemServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        if (session.getAttribute("login") == null) {
            resp.sendRedirect("access-denied.jsp");
            return;
        }
        if (req.getParameterMap().isEmpty()) {
            resp.sendRedirect("wishlist");
        } else {
            int item_id = Integer.parseInt(req.getParameter("id"));
            Database db = new Database();
            Item item = null;
            try {
                item = getInfoAboutItemById(item_id);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            db.connectToDatabase();
            try {
                String sql2 = "INSERT INTO recent_activity (user_id, activity_text, is_private) VALUES (?, ?, ?)";
                PreparedStatement statement2 = Database.getConnection().prepareStatement(sql2);
                statement2.setInt(1, item.getUser_id());
                statement2.setString(2, "removed \'" + item.getItem() + "\' from wishlist");
                statement2.setInt(3, 0);
                statement2.executeUpdate();
                String sql = "DELETE FROM wishlist WHERE item_id = ?";
                PreparedStatement statement = Database.getConnection().prepareStatement(sql);
                statement.setInt(1, item_id);
                statement.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            db.terminateConnection();
            User currentUser = (User) session.getAttribute("currentUser");
            if (currentUser.getIs_admin() == 0) {
                resp.sendRedirect("profile#wishlist");
                return;
            }
            resp.sendRedirect("wishlist");
        }
    }

    private Item getInfoAboutItemById (int item_id_to_find) throws SQLException {
        Database db = new Database();
        db.connectToDatabase();
        String sql = "SELECT user_id, item_id, item FROM wishlist WHERE item_id = ?";
        PreparedStatement statement = Database.getConnection().prepareStatement(sql);
        statement.setInt(1, item_id_to_find);
        ResultSet rs = statement.executeQuery();
        Item item_obj = null;
        while (true) {
            try {
                if (!rs.next()) {
                    break;
                }
                int user_id = rs.getInt("user_id");
                String item = rs.getString("item");
                int item_id = rs.getInt("item_id");
                item_obj = new Item(user_id,item_id, item);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        rs.close();
        db.terminateConnection();
        return item_obj;
    }
}
