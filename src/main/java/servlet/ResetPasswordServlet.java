package servlet;

import database.Database;
import model.EmailTLS;
import model.Security;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.UUID;

import static servlet.Globals.*;

@WebServlet("/ResetPassword")
public class ResetPasswordServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        session.removeAttribute("emailIsNotRegistered"); // attribute for check email validity
        if (!req.getParameterMap().isEmpty()) {
            if (req.getParameter("email") != null) { // send link to reset password
                String email = req.getParameter("email");
                System.out.println(email);
                int idByEmail = -1;
                try {
                    idByEmail = FollowUnfollowServlet.getUserIdByEmail(email);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                if (idByEmail != -1) { // entered email was found in database
                    final String uuid = UUID.randomUUID().toString().replace("-", "");
                    Database db = new Database();
                    db.connectToDatabase();
                    try {
                        String sql = "INSERT INTO verification (user_id, uuid) VALUES (?, ?)";
                        PreparedStatement statement = Database.getConnection().prepareStatement(sql);
                        statement.setInt(1, idByEmail);
                        statement.setString(2, uuid);
                        statement.executeUpdate();
                        db.terminateConnection();
                    } catch (SQLException e) {
                        e.printStackTrace();
                        resp.sendRedirect(REDIRECT_TO_ERROR_CONFIRM_LAST_ACTION);
                        return;
                    }
                    EmailTLS.sendLinkToResetPassword(email, uuid);
                    resp.sendRedirect(REDIRECT_TO_ERROR_CHECK_YOUR_EMAIL_RESET);
                } else { // entered email was not found in db
                    session.setAttribute("emailIsNotRegistered", email); // attribute for check email validity
                    resp.sendRedirect("reset-password.jsp");
                    return;
                }
            } else {
                resp.sendRedirect(REDIRECT_TO_LOGIN_PAGE);
            }

        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        session.removeAttribute("emailIsNotRegistered");
        if (!req.getParameterMap().isEmpty()) {
            if (req.getParameter("uuid") != null) { // user clicked link in email
                String uuid = req.getParameter("uuid");

                Database db = new Database();
                db.connectToDatabase();
                try {
                    String sql = "SELECT user_id FROM verification WHERE uuid = ?";
                    PreparedStatement statement = Database.getConnection().prepareStatement(sql);
                    statement.setString(1, uuid);
                    ResultSet rs = statement.executeQuery();
                    int user_id = -1;
                    while (true) {
                        try {
                            if (!rs.next()) break;
                            user_id = rs.getInt("user_id");
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                    }
                    db.terminateConnection();
                    if (user_id != -1) { // uuid is not fake
                        System.out.println("user id = " + user_id);
                        final String newRawPassword = UUID.randomUUID().toString().replace("-", ""); // generating new password
                        String generatedHash = null;
                        try {
                            db.connectToDatabase();
                            generatedHash = Security.generateHash(newRawPassword);
                            System.out.println("new raw passowrd = " + newRawPassword);
                            System.out.println("generated hash = " + generatedHash);
                            String query = "UPDATE security SET hash = ? WHERE user_id = ?";
                            PreparedStatement preparedStatement = Database.getConnection().prepareStatement(query);
                            preparedStatement.setString(1, generatedHash);
                            preparedStatement.setLong(2, user_id);
                            preparedStatement.executeUpdate();
                            String newQuery = "DELETE FROM verification WHERE uuid = ?";
                            PreparedStatement newPreparedStatement = Database.getConnection().prepareStatement(newQuery);
                            newPreparedStatement.setString(1, uuid);
                            newPreparedStatement.executeUpdate();
                            db.terminateConnection();
                        } catch (NoSuchAlgorithmException | InvalidKeySpecException | SQLException e) {
                            e.printStackTrace();
                            resp.sendRedirect(REDIRECT_TO_LOGIN_PAGE);
                            return;
                        }
                        session.setAttribute("newRawPassword", newRawPassword);
                        resp.sendRedirect(REDIRECT_TO_PASSWORD_RESETED);
                    } else {
                        resp.sendRedirect(REDIRECT_TO_LOGIN_PAGE);
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            } else {
                resp.sendRedirect(REDIRECT_TO_LOGIN_PAGE);
            }
        } else {
            resp.sendRedirect(REDIRECT_TO_LOGIN_PAGE);
        }
    }
}
