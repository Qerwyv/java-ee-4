package servlet;

public class Globals {
    public final static String REDIRECT_TO_WRONG_USERNAME_OR_PASSWORD = "redirect-to-login-page.jsp";
    public final static String REDIRECT_TO_LIST_SERVLET = "List";
    public final static String REDIRECT_TO_ACCESS_DENIED_PAGE = "access-denied.jsp";
    public final static String REDIRECT_TO_LOGIN_PAGE = "index.jsp";
    public final static String REDIRECT_TO_USER_PROFILE = "profile";
    public final static String MESSAGE_CHANGES_SUCCESSFULLY_SAVED = "changesSuccessfullySaved";
    public final static String REDIRECT_TO_CHECK_YOUR_EMAIL = "check-email-registration.jsp";
    public final static String REDIRECT_TO_ERROR_CONFIRM_YOUR_EMAIL = "confirm-email.jsp";
    public final static String REDIRECT_TO_EMAIL_CONFIRMED = "email-confirmed.jsp";
    public final static String REDIRECT_TO_PASSWORD_RESETED = "password-reseted.jsp";
    public final static String REDIRECT_TO_ERROR_CONFIRM_LAST_ACTION = "confirm-last-action.jsp";
    public final static String REDIRECT_TO_ERROR_CHECK_YOUR_EMAIL_RESET = "check-email-reset.jsp";

}
