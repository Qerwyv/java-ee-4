package servlet;

import database.Database;
import model.EmailTLS;
import model.Security;
import model.User;
import model.Users;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.UUID;

import static servlet.Globals.REDIRECT_TO_CHECK_YOUR_EMAIL;


@WebServlet("/AddUser")
public class AddUserServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        HttpSession session = req.getSession();
        session.setMaxInactiveInterval(86400);
        if (session.getAttribute("login") == null) {
            resp.sendRedirect("access-denied.jsp");
            return;
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        HttpSession session = req.getSession();
        session.setMaxInactiveInterval(86400);
        if (req.getParameterMap().containsKey("registration")) {
            session.setAttribute("registration", 1);
        } else if (session.getAttribute("login") == null) {
            resp.sendRedirect("access-denied.jsp");
            return;
        }
        if (req.getParameterMap().containsKey("name") && req.getParameterMap().containsKey("phoneNumber") &&
                req.getParameterMap().containsKey("surname") && req.getParameterMap().containsKey("email") &&
                req.getParameterMap().containsKey("password")) { // after submit
            String name = req.getParameter("name");
            String surname = req.getParameter("surname");
            String phoneNumber = req.getParameter("phoneNumber");
            String email = req.getParameter("email");
            String rawPassword = req.getParameter("password");
            String generatedHash = null;
            try {
                generatedHash = Security.generateHash(rawPassword);
            } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
                e.printStackTrace();
            }
            User newUser = new User(name, surname, phoneNumber, email);
            session.setAttribute("newUser", newUser);
            Database db = new Database();
            db.connectToDatabase();
            try {
                String sql = "SELECT * FROM user WHERE phone_number = ?";
                PreparedStatement statement = Database.getConnection().prepareStatement(sql);
                statement.setString(1, phoneNumber);
                ResultSet rsPhoneNumber = statement.executeQuery();
                if (rsPhoneNumber.next())
                    if (rsPhoneNumber.getString("phone_number").equals(phoneNumber) &&
                            rsPhoneNumber.getInt("id") != newUser.getId()) { // newUser.getId()???
                        session.setAttribute("phoneNumberExists", phoneNumber);
                        rsPhoneNumber.close();
                    } else {
                        session.removeAttribute("phoneNumberExists");
                    }
                else
                    session.removeAttribute("phoneNumberExists");
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                String sql = "SELECT * FROM user WHERE email = ?";
                PreparedStatement statement = Database.getConnection().prepareStatement(sql);
                statement.setString(1, email);
                ResultSet rsEmail = statement.executeQuery();
                if (rsEmail.next())
                    if (rsEmail.getString("email").equals(email) &&
                            rsEmail.getInt("id") != newUser.getId()) { // newUser.getId()???
                        session.setAttribute("emailExists", email);
                        rsEmail.close();
                    } else {
                        session.removeAttribute("emailExists");
                    }
                else
                    session.removeAttribute("emailExists");

            } catch (SQLException e) {
                e.printStackTrace();
            }
            if (session.getAttribute("emailExists") != null ||
                    session.getAttribute("phoneNumberExists") != null) {
                db.terminateConnection();
                if (session.getAttribute("registration") != null) {
                    resp.sendRedirect("registration.jsp");
                    return;
                }
                resp.sendRedirect("addUser.jsp");
                return;
            }
            session.removeAttribute("emailExists");
            session.removeAttribute("phoneNumberExists");
            int lastInsertedId = -1;
            try {
                String sql = "INSERT INTO user (name, surname, phone_number, email, is_admin) VALUES (?, ?, ?, ?, ?)"; // inserting new user
                PreparedStatement statement = Database.getConnection().prepareStatement(sql);
                statement.setString(1, name);
                statement.setString(2, surname);
                statement.setString(3, phoneNumber);
                statement.setString(4, email);
                if (req.getParameterMap().containsKey("admin")) {
                    statement.setInt(5, 1);
                } else {
                    statement.setInt(5, 0);
                }
                statement.executeUpdate(); // end of inserting new user
                String sql2 = "SELECT * FROM user WHERE email = ?"; // getting autoincremented id on
                PreparedStatement statement2 = Database.getConnection().prepareStatement(sql2);
                statement2.setString(1, email);
                ResultSet rsToFindId = statement2.executeQuery();
                if (rsToFindId.next())
                    lastInsertedId = rsToFindId.getInt("id");
                rsToFindId.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            if ((generatedHash != null) && (lastInsertedId != -1)) {
                String sql3 = "INSERT INTO security (user_id, hash) VALUES (?, ?)";
                try {
                    PreparedStatement statement3 = Database.getConnection().prepareStatement(sql3);
                    statement3.setInt(1, lastInsertedId);
                    statement3.setString(2, generatedHash);
                    statement3.executeUpdate();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            } else {
                System.out.println("Hash has not been generated");
            }
            db.terminateConnection();
            session.removeAttribute("newUser");
            if (session.getAttribute("registration") != null) {
                generateUUIDSendEmailWriteUUIDToDB(email, lastInsertedId);
                session.removeAttribute("registration");
                resp.sendRedirect(REDIRECT_TO_CHECK_YOUR_EMAIL);
                return;
            }
            resp.sendRedirect("List");
            return;
        }
        req.getRequestDispatcher("addUser.jsp").
                forward(req, resp);
    }

    private void generateUUIDSendEmailWriteUUIDToDB (String email, int userId) {
        final String uuid = UUID.randomUUID().toString().replace("-", "");
        Database db = new Database();
        db.connectToDatabase();
        try {
            String sql = "INSERT INTO verification VALUES (?, ?)";
            PreparedStatement statement = Database.getConnection().prepareStatement(sql);
            statement.setLong(1, userId);
            statement.setString(2, uuid);
            statement.executeUpdate();
            db.terminateConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        EmailTLS.sendConfirmationLink(email, uuid);
    }
}
