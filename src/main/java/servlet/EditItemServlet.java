package servlet;

import database.Database;
import model.Item;
import model.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static servlet.Globals.REDIRECT_TO_ACCESS_DENIED_PAGE;

@WebServlet("/EditItem")
public class EditItemServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        if (session.getAttribute("login") == null) {
            resp.sendRedirect(REDIRECT_TO_ACCESS_DENIED_PAGE);
            return;
        }
        if (req.getParameterMap().containsKey("itemForEditId")) {
            String itemForEditId = req.getParameter("itemForEditId");
            Database db = new Database();
            db.connectToDatabase();
            try {
                String sql = "SELECT * FROM wishlist WHERE item_id = ?";
                PreparedStatement statement = Database.getConnection().prepareStatement(sql);
                statement.setString(1, itemForEditId);
                ResultSet rs = statement.executeQuery();
                while (true) {
                    try {
                        if (!rs.next()) break;
                        int user_id = rs.getInt("user_id");
                        int item_id = rs.getInt("item_id");
                        String item = rs.getString("item");
                        Item currentItem = new Item(user_id, item_id, item);
                        session.setAttribute("currentItem", currentItem);
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            } catch (SQLException e) {
                e.printStackTrace();
            } db.terminateConnection();
            resp.sendRedirect("editItem.jsp");
            return;
        }
        if (req.getParameterMap().containsKey("item")) {
            Database db = new Database();
            db.connectToDatabase();
            try {
                String sql = "UPDATE wishlist SET item = ? WHERE user_id = ? AND item_id = ?";
                Item item = (Item) session.getAttribute("currentItem");
                PreparedStatement statement = Database.getConnection().prepareStatement(sql);
                statement.setString(1, req.getParameter("item"));
                statement.setLong(2, item.getUser_id());
                statement.setInt(3, item.getItem_id());
                statement.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            db.terminateConnection();
            User currentUser = (User) session.getAttribute("currentUser");
            if (currentUser.getIs_admin() == 0) {
                resp.sendRedirect("profile");
                return;
            }
            resp.sendRedirect("wishlist");
        }
    }
}
