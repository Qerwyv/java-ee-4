package servlet;

import database.Database;
import model.Security;
import model.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;

import static servlet.Globals.*;


@WebServlet("/LoginVerification")
public class LoginVerificationServlet extends HttpServlet {

    private void checkUserStatusAndRedirectHim(HttpSession session, HttpServletRequest req, HttpServletResponse resp) throws IOException {
        User currentUser = (User) session.getAttribute("currentUser");
        if (currentUser.getIs_admin() == 1) {
            System.out.println("ADMIN");
            resp.sendRedirect(REDIRECT_TO_LIST_SERVLET);
        } else {
            System.out.println("USER");
            resp.sendRedirect(REDIRECT_TO_USER_PROFILE);

        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        if (session.getAttribute("login") == null) {
            resp.sendRedirect(REDIRECT_TO_ACCESS_DENIED_PAGE);
            return;
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        session.removeAttribute("newRawPassword");
        if (req.getParameterMap().containsKey("logout")) {
            session.removeAttribute("login");
            session.removeAttribute("currentUser");
            session.removeAttribute("otherUser");
            session.removeAttribute("emailExists");
            session.removeAttribute("phoneNumberExists");
            resp.sendRedirect(REDIRECT_TO_LOGIN_PAGE);
            return;
        }
        if (!req.getParameterMap().isEmpty()) {
            String username = req.getParameter("username"); // email or phone number
            String passwordToCheck = req.getParameter("password");
            Database db = new Database();
            db.connectToDatabase();
            try {
                String sql = "SELECT * FROM user WHERE (phone_number = ?) OR (email = ?)";
                PreparedStatement statement = Database.getConnection().prepareStatement(sql);
                statement.setString(1, username);
                statement.setString(2, username);
                ResultSet rsToFindId = statement.executeQuery();
                int idToVerificate = -1;
                if (rsToFindId.next()) {
                    idToVerificate = rsToFindId.getInt("id");
                    String name = rsToFindId.getString("name");
                    String surname = rsToFindId.getString("surname");
                    String phoneNumber = rsToFindId.getString("phone_number");
                    String email = rsToFindId.getString("email");
                    String registration = rsToFindId.getString("registration");
                    int is_admin = rsToFindId.getInt("is_admin");
                    String user_pic =  rsToFindId.getString("user_pic");
                    String about =  rsToFindId.getString("about");
                    String hobbies = rsToFindId.getString("hobbies");
                    User currentUser = new User(idToVerificate, name, surname, phoneNumber,
                            email, registration, is_admin, user_pic, about, hobbies);
                    session.setAttribute("currentUser", currentUser);
                }
                rsToFindId.close();
                if (idToVerificate != -1) { // found user (id) with that email or phone number
                    String sql2 = "SELECT hash FROM security WHERE user_id = ?";
                    PreparedStatement statement2 = Database.getConnection().prepareStatement(sql2);
                    statement2.setInt(1, idToVerificate);
                    ResultSet rsToFindHash = statement2.executeQuery();
                    String hashFromDb = null;
                    if (rsToFindHash.next()) {
                        hashFromDb = rsToFindHash.getString("hash");
                    }
                    rsToFindHash.close();
                    if (hashFromDb != null) { // found hash with that id
                        boolean matched = Security.validatePassword(passwordToCheck, hashFromDb);

                        if (matched) {
                            sql = "SELECT * FROM verification WHERE user_id = ?";
                            statement = Database.getConnection().prepareStatement(sql);
                            statement.setInt(1, idToVerificate);
                            ResultSet rs = statement.executeQuery();
                            if (rs.next()) { // user isn't verified account via email link
                                resp.sendRedirect(REDIRECT_TO_ERROR_CONFIRM_YOUR_EMAIL);
                                db.terminateConnection();
                                session.removeAttribute("currentUser");
                                return;
                            }
                            session.setAttribute("login", "success");
                            checkUserStatusAndRedirectHim(session,req,resp);
                        } else {
                            db.terminateConnection();
                            resp.sendRedirect(REDIRECT_TO_WRONG_USERNAME_OR_PASSWORD);
                        }
                    } else { // no hash in db for that id. probably user was registered manually via DBMS
                        session.removeAttribute("currentUser");
                        resp.sendRedirect(REDIRECT_TO_WRONG_USERNAME_OR_PASSWORD);
                    }
                } else {
                    session.removeAttribute("currentUser");
                    resp.sendRedirect(REDIRECT_TO_WRONG_USERNAME_OR_PASSWORD);
                }
            } catch (SQLException | NoSuchAlgorithmException | InvalidKeySpecException e) {
                e.printStackTrace();
            }
        } else {
            resp.sendRedirect(REDIRECT_TO_WRONG_USERNAME_OR_PASSWORD);
        }
    }
}
