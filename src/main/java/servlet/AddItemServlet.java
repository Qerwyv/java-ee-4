package servlet;

import database.Database;
import model.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import static servlet.Globals.REDIRECT_TO_ACCESS_DENIED_PAGE;

@WebServlet("/AddItem")
public class AddItemServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        if (session.getAttribute("login") == null) {
            resp.sendRedirect(REDIRECT_TO_ACCESS_DENIED_PAGE);
            return;
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        HttpSession session = req.getSession();
        if (session.getAttribute("login") == null) {

            resp.sendRedirect(REDIRECT_TO_ACCESS_DENIED_PAGE);
            return;
        }
        if (!req.getParameterMap().isEmpty()) {
            String itemName = req.getParameter("item");
            User currentUser = (User) session.getAttribute("currentUser");
            try {
                Database db = new Database();
                db.connectToDatabase();
                String sql = "INSERT INTO wishlist (user_id, item) VALUES (?, ?)";
                PreparedStatement statement = Database.getConnection().prepareStatement(sql);
                statement.setInt(1, (int) currentUser.getId());
                statement.setString(2, itemName);
                statement.executeUpdate();
                String sql2 = "INSERT INTO recent_activity (user_id, activity_text, is_private) " +
                        "VALUES (?, ?, ?)";
                PreparedStatement statement2 = Database.getConnection().prepareStatement(sql2);
                statement2.setInt(1, (int) currentUser.getId());
                statement2.setString(2, "added \'" + itemName + "\' to wishlist");
                statement2.setInt(3, 0);
//                DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
//                Date date = new Date();
//                statement2.setString(4, dateFormat.format(date));
                statement2.executeUpdate();
                db.terminateConnection();
                if (currentUser.getIs_admin() == 0) {
                    resp.sendRedirect("profile#wishlist");
                    return;
                }
                resp.sendRedirect("wishlist");
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } else {
            resp.sendRedirect("addItem.jsp");
        }
    }
}
