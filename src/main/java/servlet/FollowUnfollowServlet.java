package servlet;

import database.Database;
import model.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static servlet.Globals.REDIRECT_TO_ACCESS_DENIED_PAGE;
import static servlet.Globals.REDIRECT_TO_USER_PROFILE;

@WebServlet("/FollowUnfollow")
public class FollowUnfollowServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println(req.getParameter("email"));
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        if (session.getAttribute("login") == null) {
            resp.sendRedirect(REDIRECT_TO_ACCESS_DENIED_PAGE);
            return;
        }
        if (!req.getParameterMap().isEmpty()) {
            User currentUser = (User) session.getAttribute("currentUser");
            Database db = new Database();
            User otherUser = null;
            int otherUserId = -1;
            if (session.getAttribute("otherUser") != null) {
                otherUser = (User) session.getAttribute("otherUser");
                try {
                    otherUserId = getUserIdByEmail(otherUser.getEmail());
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (req.getParameter("emailFollow") != null) {
                System.out.println(req.getParameter("emailFollow"));
                try {
                    int idToFollow = getUserIdByEmail(req.getParameter("emailFollow"));
                    db.connectToDatabase();
                    String sql = "INSERT INTO following (user_id, following_user_id) VALUES (?, ?)";
                    PreparedStatement statement = Database.getConnection().prepareStatement(sql);
                    statement.setLong(1, currentUser.getId());
                    if (session.getAttribute("otherUser") != null) {
                        statement.setLong(2, otherUserId);
                    } else {
                        statement.setInt(2, idToFollow);
                    }
                    statement.executeUpdate();
                    String sql2 = "INSERT INTO recent_activity (user_id, activity_text) VALUES (?, ?)";
                    PreparedStatement statement2 = Database.getConnection().prepareStatement(sql2);
                    statement2.setLong(1, currentUser.getId());
                    if (session.getAttribute("otherUser") != null) {
                        statement2.setString(2, "followed " + otherUser.getName() + " " + otherUser.getSurname()
                                + " (" + otherUser.getPhoneNumber() + ")");
                    } else {
                        User user = getUserInfoByEmail(req.getParameter("emailFollow"));
                        String name = user.getName();
                        String surname = user.getSurname();
                        String phone_number = user.getPhoneNumber();
                        statement2.setString(2, "followed " + name + " " + surname
                                + " (" + phone_number + ")");
                    }
                    statement2.executeUpdate();
                    db.terminateConnection();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            } else if (req.getParameter("emailUnfollow") != null) {
                System.out.println(req.getParameter("emailUnfollow"));
                try {
                    int idToUnfollow = getUserIdByEmail(req.getParameter("emailUnfollow"));
                    db.connectToDatabase();
                    String sql = "DELETE FROM following WHERE user_id = ? AND following_user_id = ?";
                    PreparedStatement statement = Database.getConnection().prepareStatement(sql);
                    statement.setLong(1, currentUser.getId());
                    statement.setInt(2, idToUnfollow);
                    statement.executeUpdate();
                    String sql2 = "INSERT INTO recent_activity (user_id, activity_text) VALUES (?, ?)";
                    PreparedStatement statement2 = Database.getConnection().prepareStatement(sql2);
                    statement2.setLong(1, currentUser.getId());
                    if (session.getAttribute("otherUser") != null) {
                        statement2.setString(2, "unfollowed " + otherUser.getName() + " " + otherUser.getSurname()
                                + " (" + otherUser.getPhoneNumber() + ")");
                    } else {
                        User user = getUserInfoByEmail(req.getParameter("emailUnfollow"));
                        String name = user.getName();
                        String surname = user.getSurname();
                        String phone_number = user.getPhoneNumber();
                        statement2.setString(2, "unfollowed " + name + " " + surname
                                + " (" + phone_number + ")");
                    }
                    statement2.executeUpdate();
                    db.terminateConnection();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (session.getAttribute("otherUser") != null) {
                if (otherUser != null) {
                    resp.sendRedirect("profile?email=" + otherUser.getEmail());
                }
                return;
            }
            resp.sendRedirect("profile#following");
            return;
        } else {
            resp.sendRedirect(REDIRECT_TO_USER_PROFILE);
        }
    }

    public static User getUserInfoByEmail(String email) throws SQLException {
        Database db = new Database();
        db.connectToDatabase();
        String sql = "SELECT id, name, surname, phone_number FROM user WHERE email = ?";
        PreparedStatement statement = Database.getConnection().prepareStatement(sql);
        statement.setString(1, email);
        ResultSet rs = statement.executeQuery();
        User user = null;
        while (true) {
            try {
                if (!rs.next()) {
                    break;
                }
                int id = rs.getInt("id");
                String name = rs.getString("name");
                String surname = rs.getString("surname");
                String phone_number = rs.getString("phone_number");
                user = new User(id, name, surname, phone_number);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        rs.close();
        db.terminateConnection();
        return user;
    }


    public static int getUserIdByEmail(String email) throws SQLException {
        Database db = new Database();
        db.connectToDatabase();
        String sql = "SELECT id FROM user WHERE email = ?";
        PreparedStatement statement = Database.getConnection().prepareStatement(sql);
        statement.setString(1, email);
        ResultSet rs = statement.executeQuery();
        int userId = -1;
        while (true) {
            try {
                if (!rs.next()) {
                    break;
                }
                userId = rs.getInt("id");
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        rs.close();
        db.terminateConnection();
        return userId;
    }
}
