package servlet;

import database.Database;
import model.Activity;
import model.Item;
import model.Security;
import model.User;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static servlet.Globals.MESSAGE_CHANGES_SUCCESSFULLY_SAVED;
import static servlet.Globals.REDIRECT_TO_ACCESS_DENIED_PAGE;

@WebServlet("/profile")
public class UserProfileServlet extends HttpServlet {

    private void updateDataAboutCurrentUser(Database db, HttpSession session) {
        db.connectToDatabase();
        try {
            String sql = "SELECT * FROM user WHERE id = ?";
            PreparedStatement statement = Database.getConnection().prepareStatement(sql);
            User currentUserOldData = (User) session.getAttribute("currentUser");
            int currentUserId = (int) currentUserOldData.getId();
            statement.setInt(1, currentUserId);
            ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                int id = rs.getInt("id");
                String name = rs.getString("name");
                String surname = rs.getString("surname");
                String phoneNumber = rs.getString("phone_number");
                String email = rs.getString("email");
                String registration = rs.getString("registration");
                int is_admin = rs.getInt("is_admin");
                String user_pic = rs.getString("user_pic");
                String about = rs.getString("about");
                String hobbies = rs.getString("hobbies");
                User currentUser = new User(id, name, surname, phoneNumber,
                        email, registration, is_admin, user_pic, about, hobbies);
                session.setAttribute("currentUser", currentUser);
            }
            rs.close();
            db.terminateConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void getCurrentUserFollowing(Database db, HttpSession session) {
        db.connectToDatabase();
        ResultSet rs = null;
        User currentUser = (User) session.getAttribute("currentUser");
        int currentUserId = (int) currentUser.getId();
        try {
            String sql = "SELECT name, surname, phone_number, email FROM user " +
                    "INNER JOIN following ON user.id = following.following_user_id " +
                    "WHERE following.user_id = ?";
            PreparedStatement statement = Database.getConnection().prepareStatement(sql);
            statement.setInt(1, currentUserId);
            rs = statement.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        List<User> listOfFollowing = new ArrayList<>();
        while (true) {
            try {
                if (!rs.next()) break;
                String name = rs.getString("name");
                String surname = rs.getString("surname");
                String phoneNumber = rs.getString("phone_number");
                String email = rs.getString("email");
                User user = new User(name, surname, phoneNumber, email);
                listOfFollowing.add(user);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        session.setAttribute("listOfFollowing", listOfFollowing);
        db.terminateConnection();
    }

    private void getCurrentUserFollowers(Database db, HttpSession session) {
        db.connectToDatabase();
        ResultSet rs = null;
        User currentUser = (User) session.getAttribute("currentUser");
        int currentUserId = (int) currentUser.getId();
        try {
            String sql = "SELECT user_id FROM following WHERE following_user_id = ?";
            PreparedStatement statement = Database.getConnection().prepareStatement(sql);
            statement.setInt(1, currentUserId);
            rs = statement.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        int numberOfFollowers = 0;
        while (true) {
            try {
                if (!rs.next()) break;
                int user_id = rs.getInt("user_id");
                numberOfFollowers++;
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        session.setAttribute("numberOfFollowers", numberOfFollowers);
        db.terminateConnection();
    }


    private void getCurrentUserItems(Database db, HttpSession session) {
        db.connectToDatabase();
        try {
            String sql = "SELECT item_id, item FROM wishlist WHERE user_id = ?";
            PreparedStatement statement = Database.getConnection().prepareStatement(sql);
            User currentUserOldData = (User) session.getAttribute("currentUser");
            int currentUserId = (int) currentUserOldData.getId();
            statement.setInt(1, currentUserId);
            ResultSet rs = statement.executeQuery();
            List<Item> wishlist = new ArrayList<>();
            while (true) {
                try {
                    if (!rs.next()) {
                        break;
                    }
                    int item_id = rs.getInt("item_id");
                    String item = rs.getString("item");
                    Item itemObject = new Item(item_id, item);
                    wishlist.add(itemObject);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            session.setAttribute("wishlist", wishlist);
            db.terminateConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void getCurrentUserRecentActivity (Database db, HttpSession session) {
        db.connectToDatabase();
        try {
            String sql = "SELECT activity_text, datetime FROM recent_activity WHERE user_id = ? ORDER BY datetime DESC LIMIT 10";
            PreparedStatement statement = Database.getConnection().prepareStatement(sql);
            User currentUserOldData = (User) session.getAttribute("currentUser");
            int currentUserId = (int) currentUserOldData.getId();
            statement.setInt(1, currentUserId);
            ResultSet rs = statement.executeQuery();
            List<Activity> recent_activity = new ArrayList<>();
            while (true) {
                try {
                    if (!rs.next()) {
                        break;
                    }
                    String activity_text = rs.getString("activity_text");
                    String datetime = rs.getString("datetime");
                    Activity activity = new Activity(activity_text, datetime);
                    recent_activity.add(activity);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            session.setAttribute("recent_activity", recent_activity);
            db.terminateConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void getOtherUserRecentActivity (Database db, HttpSession session, int otherUserId) {
        db.connectToDatabase();
        try {
            String sql = "SELECT activity_text, datetime FROM recent_activity WHERE user_id = ?";
            PreparedStatement statement = Database.getConnection().prepareStatement(sql);
            statement.setInt(1, otherUserId);
            ResultSet rs = statement.executeQuery();
            List<Activity> recent_activity = new ArrayList<>();
            while (true) {
                try {
                    if (!rs.next()) {
                        break;
                    }
                    String activity_text = rs.getString("activity_text");
                    String datetime = rs.getString("datetime");
                    Activity activity = new Activity(activity_text, datetime);
                    recent_activity.add(activity);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            session.setAttribute("otherUserRecentActivity", recent_activity);
            db.terminateConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    private void searchUsers(Database db, HttpSession session, String sequence) {
        String[] splited = sequence.split("\\s+"); // index 0 - name, 1 - surname
        session.setAttribute("search", 1);
        session.setAttribute("searchSequence", sequence);
        db.connectToDatabase();
        ResultSet rs = null;
        User currentUser = (User) session.getAttribute("currentUser");
        int currentUserId = (int) currentUser.getId();
        try {
            String sql = null;
            if ((splited.length > 1) &&
                    ((splited[0].toCharArray()[0] != ' ') || (splited[1].toCharArray()[0] != ' ')
                            || (splited[2].toCharArray()[0] != ' '))) {
                sql = "SELECT name, surname, phone_number, email FROM user " +
                        "WHERE (name LIKE ? AND surname LIKE ?) OR (name LIKE ? AND surname LIKE ?)" +
                        "EXCEPT SELECT name, surname, phone_number, email FROM user WHERE email = ?";
                PreparedStatement statement = Database.getConnection().prepareStatement(sql);
                statement.setString(1, "%" + splited[0] + "%");
                statement.setString(2, "%" + splited[1] + "%");
                statement.setString(3, "%" + splited[1] + "%");
                statement.setString(4, "%" + splited[0] + "%");
                statement.setString(5, currentUser.getEmail());
                rs = statement.executeQuery();
            } else if (splited.length == 1) {
                sql = "SELECT name, surname, phone_number, email FROM user " +
                        "WHERE email LIKE ? OR phone_number LIKE ? OR name LIKE ? OR surname LIKE ?" +
                        "EXCEPT SELECT name, surname, phone_number, email FROM user WHERE email = ?";
                PreparedStatement statement = Database.getConnection().prepareStatement(sql);
                statement.setString(1, "%" + sequence + "%");
                statement.setString(2, "%" + sequence + "%");
                statement.setString(3, "%" + sequence + "%");
                statement.setString(4, "%" + sequence + "%");
                statement.setString(5, currentUser.getEmail());
                rs = statement.executeQuery();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        List<User> searchList = new ArrayList<>();
        while (true) {
            try {
                if (!rs.next()) break;
                String name = rs.getString("name");
                String surname = rs.getString("surname");
                String phoneNumber = rs.getString("phone_number");
                String email = rs.getString("email");
                User user = new User(name, surname, phoneNumber, email);
                searchList.add(user);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        session.setAttribute("searchList", searchList);
        db.terminateConnection();
    }

    private void showMessageAboutSuccessfullySavedChanges(HttpSession session) {
        if (session.getAttribute(MESSAGE_CHANGES_SUCCESSFULLY_SAVED) != null) {
            if (session.getAttribute(MESSAGE_CHANGES_SUCCESSFULLY_SAVED).equals(2)) {
                session.removeAttribute(MESSAGE_CHANGES_SUCCESSFULLY_SAVED);
            }
            if (session.getAttribute(MESSAGE_CHANGES_SUCCESSFULLY_SAVED) != null) {
                if (session.getAttribute(MESSAGE_CHANGES_SUCCESSFULLY_SAVED).equals(1)) {
                    session.setAttribute(MESSAGE_CHANGES_SUCCESSFULLY_SAVED, 2);
                }
            }
        }
    }

    private int getDataByEmail (HttpSession session, Database db, String email) {
        db.connectToDatabase();
        try {
            String sql = "SELECT * FROM user WHERE email = ?";
            PreparedStatement statement = Database.getConnection().prepareStatement(sql);
            statement.setString(1, email);
            ResultSet rs = statement.executeQuery();
            int otherUserId = -1;
            while (true) {
                try {
                    if (!rs.next()) {
                        break;
                    }
                    otherUserId = rs.getInt("id");
                    String name = rs.getString("name");
                    String surname = rs.getString("surname");
                    String phoneNumber = rs.getString("phone_number");
                    String user_pic = rs.getString("user_pic");
                    String about = rs.getString("about");
                    String hobbies = rs.getString("hobbies");
                    User user = new User(name, surname, phoneNumber,
                            email, user_pic, about, hobbies);
                    session.setAttribute("otherUser", user);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (otherUserId != -1) {
                sql = "SELECT * FROM wishlist WHERE user_id = ?";
                statement = Database.getConnection().prepareStatement(sql);
                statement.setInt(1, otherUserId);
                rs = statement.executeQuery();
                List<Item> wishlist = new ArrayList<>();
                while (true) {
                    try {
                        if (!rs.next()) {
                            break;
                        }
                        int item_id = rs.getInt("item_id");
                        String item = rs.getString("item");
                        Item itemObject = new Item(item_id, item);
                        wishlist.add(itemObject);
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
                session.setAttribute("otherUserWishlist", wishlist);
            }
            db.terminateConnection();
            return otherUserId;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }



    // ------- POST

    private void checkDataForEdit(HttpServletRequest req, HttpServletResponse resp,
                                  HttpSession session, Database db) throws IOException {
        User currentUser = (User) session.getAttribute("currentUser");
        User userForEdit = new User();
        String email = req.getParameter("email");
        String phoneNumber = req.getParameter("phoneNumber");
        userForEdit.setEmail(email);
        userForEdit.setPhoneNumber(phoneNumber);
        userForEdit.setName(req.getParameter("name"));
        userForEdit.setSurname(req.getParameter("surname"));
        userForEdit.setAbout(req.getParameter("about"));
        userForEdit.setHobbies(req.getParameter("hobbies"));
        session.setAttribute("userForEdit", userForEdit);
        db.connectToDatabase();
        try {
            String sql = "SELECT * FROM user WHERE phone_number = ?";
            PreparedStatement statement = Database.getConnection().prepareStatement(sql);
            statement.setString(1, phoneNumber);
            ResultSet rsPhoneNumber = statement.executeQuery();
            if (rsPhoneNumber.next())
                if (rsPhoneNumber.getString("phone_number").equals(phoneNumber) &&
                        rsPhoneNumber.getInt("id") != currentUser.getId()) {
                    session.setAttribute("phoneNumberExists", phoneNumber);
                    rsPhoneNumber.close();
                } else {
                    session.removeAttribute("phoneNumberExists");
                }
            else
                session.removeAttribute("phoneNumberExists");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        try {
            String sql = "SELECT * FROM user WHERE email = ?";
            PreparedStatement statement = Database.getConnection().prepareStatement(sql);
            statement.setString(1, email);
            ResultSet rsEmail = statement.executeQuery();
            if (rsEmail.next())
                if (rsEmail.getString("email").equals(email) &&
                        rsEmail.getInt("id") != currentUser.getId()) {
                    session.setAttribute("emailExists", email);
                    rsEmail.close();
                } else {
                    session.removeAttribute("emailExists");
                }
            else
                session.removeAttribute("emailExists");

        } catch (SQLException e) {
            e.printStackTrace();
        }
        if (session.getAttribute("emailExists") != null ||
                session.getAttribute("phoneNumberExists") != null) {
            db.terminateConnection();
            resp.sendRedirect("profile#edit");
            return;
        }
        session.removeAttribute("emailExists");
        session.removeAttribute("phoneNumberExists");
        if (req.getParameter("password") != null && !req.getParameter("password").equals("")) {
            String rawPassword = req.getParameter("password");
            String generatedHash = null;
            try {
                generatedHash = Security.generateHash(rawPassword);
                String sql = "UPDATE security SET hash = ? WHERE user_id = ?";
                PreparedStatement statement = Database.getConnection().prepareStatement(sql);
                statement.setString(1, generatedHash);
                statement.setLong(2, currentUser.getId());
                statement.executeUpdate();
            } catch (NoSuchAlgorithmException | InvalidKeySpecException | SQLException e) {
                e.printStackTrace();
            }
        }
        try {
            String sql = "UPDATE user SET name = ?, surname = ?, phone_number = ?, " +
                    "email = ?, about = ?, hobbies = ? WHERE id = ?";
            PreparedStatement statement = Database.getConnection().prepareStatement(sql);
            statement.setString(1, userForEdit.getName());
            statement.setString(2, userForEdit.getSurname());
            statement.setString(3, phoneNumber);
            statement.setString(4, email);
            statement.setString(5, userForEdit.getAbout());
            statement.setString(6, userForEdit.getHobbies());
            statement.setLong(7, currentUser.getId());
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        session.removeAttribute("userForEdit");
        db.terminateConnection();
        resp.sendRedirect("profile");
        session.setAttribute("changesSuccessfullySaved", 1);
    }


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        HttpSession session = req.getSession();
        if (session.getAttribute("emailIsNotRegistered")!=null) {
            session.removeAttribute("emailIsNotRegistered");
        }
        if (session.getAttribute("login") == null) {
            resp.sendRedirect(REDIRECT_TO_ACCESS_DENIED_PAGE);
            return;
        }
        Database db = new Database();
        updateDataAboutCurrentUser(db, session);
        getCurrentUserFollowing(db, session);
        getCurrentUserFollowers(db, session);
        getCurrentUserItems(db, session);
        getCurrentUserRecentActivity(db, session);
        showMessageAboutSuccessfullySavedChanges(session);
        if (!req.getParameterMap().isEmpty()) {
            if (req.getParameter("email") != null) { // open other user profile
                String otherUserEmail = req.getParameter("email");
                int otherUserId = getDataByEmail (session, db, otherUserEmail);
                if (otherUserId != -1) {
                    getOtherUserRecentActivity(db, session, otherUserId);
                }
                RequestDispatcher dispatcher = getServletContext()
                        .getRequestDispatcher("/other-user-profile.jsp");
                dispatcher.forward(req, resp);
            }
            if (req.getParameter("searchUserField") != null) {
                String sequence = req.getParameter("searchUserField");
                searchUsers(db, session, sequence);
            } else {
                session.removeAttribute("search");
            }
        } else {
            session.removeAttribute("otherUser");
            session.removeAttribute("search");
        }
        RequestDispatcher dispatcher = getServletContext()
                .getRequestDispatcher("/user-profile.jsp");
        dispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");
        HttpSession session = req.getSession();
        Database db = new Database();
        showMessageAboutSuccessfullySavedChanges(session);
        checkDataForEdit(req, resp, session, db);
//        RequestDispatcher dispatcher = getServletContext()
//                .getRequestDispatcher("/user-profile.jsp");
//        dispatcher.forward(req, resp);
    }
}
