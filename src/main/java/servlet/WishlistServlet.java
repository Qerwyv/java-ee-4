package servlet;

import database.Database;
import model.Item;
import model.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static servlet.Globals.REDIRECT_TO_ACCESS_DENIED_PAGE;

@WebServlet("/wishlist")
public class WishlistServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        if (session.getAttribute("login") == null) {
            resp.sendRedirect(REDIRECT_TO_ACCESS_DENIED_PAGE);
            return;
        }
        session.removeAttribute("wishlist");
        Database db = new Database();
        db.connectToDatabase();
        ResultSet rs = null;
        try {
            String sql = "SELECT * FROM wishlist WHERE user_id = ?";
            PreparedStatement statement = Database.getConnection().prepareStatement(sql);
            if (!req.getParameterMap().isEmpty()) {
                statement.setString(1, req.getParameter("id"));
            } else {
                User currentUser = (User) session.getAttribute("currentUser");
                long currentUserId = currentUser.getId();
                statement.setLong(1, currentUserId);
            }
            rs = statement.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        List<Item> wishlist = new ArrayList<>();
        while (true) {
            try {
                if (!rs.next()) {
                    break;
                }
                int user_id = rs.getInt("user_id");
                int item_id = rs.getInt("item_id");
                String item = rs.getString("item");
                Item itemObject = new Item(user_id, item_id, item);
                wishlist.add(itemObject);
                session.setAttribute("wishlist", wishlist);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        rs = null;
        try {
            String sql = "SELECT * FROM user WHERE id = ?";
            PreparedStatement statement = Database.getConnection().prepareStatement(sql);
            statement.setString(1, req.getParameter("id"));
            rs = statement.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        while (true) {
            try {
                if (!rs.next()) break;
                int id = rs.getInt("id");
                String name = rs.getString("name");
                String surname = rs.getString("surname");
                String phoneNumber = rs.getString("phone_number");
                String email = rs.getString("email");
                User currentUser = new User(id, name, surname, phoneNumber, email);
                session.setAttribute("currentUser", currentUser);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        db.terminateConnection();
        resp.sendRedirect("wishlist.jsp");
    }
}
