package servlet;

import database.Database;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import static servlet.Globals.REDIRECT_TO_EMAIL_CONFIRMED;

@WebServlet("/registration")
public class RegistrationServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String uuid = req.getParameter("uuid");
        Database db = new Database();
        db.connectToDatabase();
        try {
            String sql = "DELETE FROM verification WHERE uuid = ?";
            PreparedStatement statement = Database.getConnection().prepareStatement(sql);
            statement.setString(1, uuid);
            statement.executeUpdate();
            db.terminateConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        resp.sendRedirect(REDIRECT_TO_EMAIL_CONFIRMED);
        return;
    }
}
