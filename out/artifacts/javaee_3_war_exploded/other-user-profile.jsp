<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<html lang="en">
<head>
    <c:if test="${error != null}">
        ${error = null}
    </c:if>
    <c:if test="${login == null}">
        <script>window.location.href = 'access-denied.jsp'</script>
    </c:if>
    <c:if test="${login != null}">
        <%@ page contentType="text/html;charset=UTF-8" language="java" %>
        <%--        <jsp:useBean id="listOfUsersDB" scope="session" type="java.util.ArrayList"/>--%>
        <jsp:useBean id="otherUser" scope="session" type="model.User"/>
        <jsp:useBean id="otherUserRecentActivity" scope="session" type="java.util.ArrayList"/>
        <jsp:useBean id="otherUserWishlist" scope="session" type="java.util.ArrayList"/>
        <jsp:useBean id="listOfFollowing" scope="session" type="java.util.ArrayList"/>

    </c:if>
    <meta charset="UTF-8">
    <title>Title</title>

    <link rel="stylesheet" href="resources/css/bootstrap.css">
    <link rel="stylesheet" href="resources/css/bootstrap-grid.css">
    <link rel="stylesheet" href="resources/css/bootstrap-reboot.css">
    <link rel="stylesheet" href="resources/js/bootstrap.js">
    <link rel="stylesheet" href="resources/js/bootstrap.bundle.js">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="resources/style.css">

    <script src="resources/js/jquery-3.4.1.min.js"></script>
    <script src="resources/js/popper.min.js"></script>
    <script src="resources/js/bootstrap.js"></script>


</head>
<body>

<div class="container">
    <div class="row my-2">
        <div class="col-lg-8 order-lg-2">
            <ul class="nav nav-tabs">
                <li class="nav-item">
                    <a href="#profile" data-target="#profile" data-toggle="tab" class="nav-link active">Profile</a>
                </li>
                <li class="nav-item">
                    <a href="#wishlist" data-target="#wishlist" data-toggle="tab" class="nav-link">Wishlist</a>
                </li>
                <li class="nav-item ml-auto" style="right: 0px">
                    <form method="get" action="profile">
                        <input type="submit" class="nav-link btn btn-outline-info" value="Get back to my profile"/>
                    </form>
                </li>
            </ul>

            <div class="tab-content py-4">

                <div class="tab-pane active" id="profile">
                    <h5 class="mb-3">${otherUser.name} ${otherUser.surname}</h5> <!-- username -->
                    <div class="row">
                        <div class="col-md-6">
                            ${otherUser.phoneNumber}
                            <br>
                                ${otherUser.email}
                            <br>
                            <br>
                            <h6>About</h6>
                            <p>
                                <c:if test="${not empty otherUser.about}">
                                    ${otherUser.about}
                                </c:if>
                                <c:if test="${empty otherUser.about}">
                                    No additional details to show.
                                </c:if>
                            </p>
                            <h6>Hobbies</h6>
                            <p>
                                <c:if test="${not empty otherUser.hobbies}">
                                    ${otherUser.hobbies}
                                </c:if>
                                <c:if test="${empty otherUser.hobbies}">
                                    No additional details to show.
                                </c:if>
                            </p>
                        </div>
                        <div class="col-md-6">
                            <%--                            <h6>Recent badges</h6>--%>
                            <%--                            <a href="#" class="badge badge-dark badge-pill">html5</a>--%>
                            <%--                            <a href="#" class="badge badge-dark badge-pill">react</a>--%>
                            <%--                            <a href="#" class="badge badge-dark badge-pill">codeply</a>--%>
                            <%--                            <a href="#" class="badge badge-dark badge-pill">angularjs</a>--%>
                            <%--                            <a href="#" class="badge badge-dark badge-pill">css3</a>--%>
                            <%--                            <a href="#" class="badge badge-dark badge-pill">jquery</a>--%>
                            <%--                            <a href="#" class="badge badge-dark badge-pill">bootstrap</a>--%>
                            <%--                            <a href="#" class="badge badge-dark badge-pill">responsive-design</a>--%>
                            <%--                            <hr>--%>
                            <%--                                <c:url value="FollowUnfollow" var="Follow">--%>
                            <%--                                    <c:param name="emailFollow" value="${otherUser.email}"/>--%>
                            <%--                                </c:url>--%>
                            <%--                                <a class="btn btn-outline-primary btn-sm"--%>
                            <%--                                   onclick="return confirm('Are you sure?')"--%>
                            <%--                                   role="button" href="${Follow}">Follow     <span class="badge badge-primary"><i--%>
                            <%--                                        class="fa fa-check"></i></span></a>--%>

                            <c:set var="contains" value="false"/>
                            <c:forEach var="followingUser" items="${listOfFollowing}">
                                <c:if test="${otherUser.email eq followingUser.email}">
                                    <c:set var="contains" value="true"/>
                                    <!-- this user is already followed by current user
                                    so change "follow' to 'unfollow' -->
                                    <c:url value="FollowUnfollow" var="Unfollow">
                                        <c:param name="emailUnfollow" value="${otherUser.email}"/>
                                    </c:url>
                                    <a class="btn btn-outline-primary btn-sm"
                                       onclick="return confirm('Are you sure?')"
                                       role="button" href="${Unfollow}">Unfollow <span class="badge badge-primary"><i
                                            class="fa fa-check"></i></span></a>
                                </c:if>
                            </c:forEach>
                            <c:if test="${contains eq false}">
                                <c:url value="FollowUnfollow" var="Follow">
                                    <c:param name="emailFollow" value="${otherUser.email}"/>
                                </c:url>
                                <a class="btn btn-outline-primary btn-sm"
                                   onclick="return confirm('Are you sure?')"
                                   role="button" href="${Follow}">Follow <span class="badge badge-primary"><i
                                        class="fa fa-check"></i></span></a>
                            </c:if>

                            <br><br>
                            <span class="badge badge-success"><i class="fa fa-cog"></i> ${fn:length(otherUserWishlist)} Items in wishlist</span>

                        </div>
                        <div class="col-md-12">
                            <h5 class="mt-2"><span class="fa fa-clock-o ion-clock float-right"></span> Recent Activity
                            </h5>
                            <table class="table table-sm table-hover table-striped">
                                <tbody>
                                <c:forEach items="${otherUserRecentActivity}" var="activity">
                                    <tr>
                                        <td>
                                            <strong><c:out value="${otherUser.name}"></c:out></strong>
                                            <c:out value="${activity.text}"> </c:out>
                                        </td>
                                    </tr>
                                </c:forEach>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!--/row-->
                </div>

                <div class="tab-pane" id="wishlist">
                    <%--                    <div class="alert alert-info alert-dismissable">--%>
                    <%--                        <a class="panel-close close" data-dismiss="alert">×</a> This is an <strong>.alert</strong>. Use--%>
                    <%--                        this to show important messages to the user.--%>
                    <%--                    </div>--%>
                    <c:if test="${fn:length(otherUserWishlist) == 0}">
                        <div class="alert alert-info alert-dismissable">
                            <a class="panel-close close" data-dismiss="alert">×</a>
                            <strong>${otherUser.name}</strong> don't have any items in wishlist :(
                        </div>
                    </c:if>
                    <c:if test="${fn:length(otherUserWishlist) != 0}">
                        <table class="table table-striped">
                            <thead>
                            <tr class="table-success">
                                <th scope="col">Id</th>
                                <th scope="col">Item</th>
                            </tr>
                            </thead>
                            <tbody>
                            <c:forEach items="${otherUserWishlist}" var="item">
                                <tr>
                                    <th scope="row">
                                        <c:out value="${item.item_id}"></c:out>
                                    </th> <!-- ID -->
                                    <td>
                                        <c:out value="${item.item}"></c:out>
                                    </td>
                                </tr>
                            </c:forEach>
                            </tbody>
                        </table>
                    </c:if>
                </div>

                <script>
                    var hash = window.location.hash;
                    hash && $('ul.nav a[href="' + hash + '"]').tab('show');
                    $('.nav-tabs a').click(function (e) {
                        $(this).tab('show');
                        var scrollmem = $('body').scrollTop();
                        window.location.hash = this.hash;
                        $('html,body').scrollTop(scrollmem);
                    });
                </script>
            </div>
        </div>
        <div class="col-lg-4 order-lg-1 text-center">
            <img src="https://cdn3.iconfinder.com/data/icons/flat-design-hands-icons/128/31-256.png"
                 class="mx-auto img-fluid img-circle d-block" alt="avatar">
        </div>
    </div>
</div>
</body>
<script>
    $(document).ready(function () {
        $(".nav-tabs a").click(function () {
            $(this).tab('show');
        });
    });
</script>
</html>