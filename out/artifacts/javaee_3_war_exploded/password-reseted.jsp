
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

<head>
    <title>Password reseted</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="resources/css/bootstrap.min.css"/>
    <c:if test="${newRawPassword != null}">
        <jsp:useBean id="newRawPassword" scope="session" type="java.lang.String"/>
    </c:if>
</head>
<body>
<div class="alert alert-primary text-center" role="alert">
    <h2>Password was successfully reseted!<br></h2>
</div>
<div class="text-center">
    Your new password is: <strong style="color: #fa4251">${newRawPassword}</strong> <br>
    Now you can <a href="index.jsp">log in</a> using your credentials. <br>
    We are <strong>strongly</strong> recommend you to change this password after <a href="index.jsp">logging in</a>.

</div>
</body>
</html>
