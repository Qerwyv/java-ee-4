<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html lang="en">
<head>
    <c:if test="${emailIsNotRegistered != null}">
        <%--        <jsp:useBean id="listOfUsersDB" scope="session" type="java.util.ArrayList"/>--%>
        <jsp:useBean id="emailIsNotRegistered" scope="session" type="java.lang.String"/>
    </c:if>
    <meta charset="UTF-8">
    <title>Gifter Reset password</title>
    <link href="resources/css/bootstrap-3.3.0/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="resources/js/bootstrap.min.js"></script>
    <script src="resources/js/jquery-3.4.1.min.js"></script>
</head>
<body>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<div class="form-gap"></div>
<div style="text-align: center;">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="text-center">
                            <h3><i class="fa fa-lock fa-4x"></i></h3>
                            <h2 class="text-center">Forgot Password?</h2>
                            <p>You can reset your password here.</p>
                            <div class="panel-body">
                                <form id="register-form" role="form" autocomplete="off" class="form" method="post"
                                      action="ResetPassword">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon"><i
                                                    class="glyphicon glyphicon-envelope color-blue"></i></span>
                                            <input id="email" name="email" placeholder="Email address"
                                                   class="form-control" type="email" required value="${emailIsNotRegistered}">
                                            <span id="message_about_email"></span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <input name="recover-submit" class="btn btn-lg btn-primary btn-block"
                                               value="Reset Password" type="submit">
                                    </div>
                                    <c:if test="${emailIsNotRegistered != null}">
                                        <strong style="color: red">There is no user registered with that email</strong>
                                    </c:if>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>