<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="en">

<head>
    <c:if test="${error != null}">
        <jsp:useBean id="newUser" scope="session" type="model.User"/>
    </c:if>
    <c:if test="${login != null}">
        <script>window.location.href = 'profile'</script>
    </c:if>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Colorlib Templates">
    <meta name="author" content="Colorlib">
    <meta name="keywords" content="Colorlib Templates">

    <!-- Title Page-->
    <title>Gifter Registration</title>

    <!-- Icons font CSS-->
    <link href="resources/regform/vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">
    <link href="resources/regform/vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <!-- Font special for pages-->
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
          rel="stylesheet">

    <!-- Vendor CSS-->
    <link href="resources/regform/vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="resources/regform/vendor/datepicker/daterangepicker.css" rel="stylesheet" media="all">

    <link href="resources/loginform/css/util.css" rel="stylesheet" media="all">
    <!-- Main CSS-->
    <link href="resources/regform/css/main.css" rel="stylesheet" media="all">
</head>

<body>
<div class="page-wrapper bg-gra-02 p-t-115 p-b-110 font-poppins">
    <div class="wrapper wrapper--w680 ">
        <div class="card card-4 ">
            <div class="card-body" style="padding: 30px 30px;">
                <h2 class="title">Registration Form</h2>
                <form method="POST" action="AddUser">
                    <div class="row row-space">
                        <div class="col-2">
                            <div class="input-group">
                                <label class="label">first name</label>
                                <input class="input--style-4" type="text"
                                       name="name" value="${newUser.name}" required>
                                <!-- TODO autocomplete="off" after testing-->
                            </div>
                        </div>
                        <div class="col-2">
                            <div class="input-group">
                                <label class="label">last name</label>
                                <input class="input--style-4" type="text"
                                       name="surname" value="${newUser.surname}" required>
                            </div>
                        </div>
                    </div>
                    <div class="row row-space">
                        <div class="col-2">
                            <c:if test="${emailExists == null}">
                                <div class="input-group">
                                    <label class="label">Email</label>
                                    <input class="input--style-4" type="email"
                                           name="email" value="${newUser.email}" required>
                                </div>
                            </c:if>
                            <c:if test="${emailExists != null}">
                                <div class="input-group">
                                    <label class="label">Email</label>
                                    <input class="input--style-4" type="email"
                                           name="email" value="${emailExists}" required>
                                    <div class="invalid-feedback">
                                        This email address is already registered
                                    </div>
                                </div>
                            </c:if>
                        </div>
                        <div class="col-2">
                            <div class="input-group">
                                <label class="label">Phone Number</label>
                                <c:if test="${phoneNumberExists == null}">
                                    <input class="input--style-4" type="text" name="phoneNumber"
                                           pattern="[+][0-9]{7,15}"
                                           oninvalid="this.setCustomValidity('Your input does not match field requirements. Phone number must ' +
                                        'begins with \'+\' and have at least 7 characters')"
                                           oninput="this.setCustomValidity('')"
                                           value="${newUser.phoneNumber}"
                                           required>
                                </c:if>
                                <c:if test="${phoneNumberExists != null}">
                                    <input class="input--style-4" type="text" name="phoneNumber"
                                           pattern="[+][0-9]{7,15}"
                                           oninvalid="this.setCustomValidity('Your input does not match field requirements. Phone number must ' +
                                        'begins with \'+\' and have at least 7 characters')"
                                           oninput="this.setCustomValidity('')"
                                           value="${phoneNumberExists}"
                                           required>
                                    <div class="invalid-feedback">
                                        This phone number is already registered
                                    </div>
                                </c:if>
                            </div>
                        </div>
                    </div>
                    <div class="row row-space">
                        <div class="col-2">
                            <div class="input-group">
                                <label class="label">Password</label>
                                <input class="input--style-4" type="password" minlength="8" name="password"
                                       id="password" required
                                       oninvalid="this.setCustomValidity('Your input does not match field requirements. ' +
                                        'Password must be at least 8 characters long')"
                                       oninput="this.setCustomValidity('')"
                                       onkeyup="checkPasswordMatching();">
                            </div>
                        </div>
                        <div class="col-2">
                            <div class="input-group">
                                <label class="label">Password confirmation</label>
                                <input class="input--style-4" type="password"
                                       id="password_confirmation" required onkeyup="checkPasswordMatching();">
                                <span id="message_about_password"></span>

                            </div>
                        </div>
                    </div>
                    <script type="text/javascript">
                        var checkPasswordMatching = function () {
                            var password = document.getElementById('password').value;
                            var password_confirmation = document.getElementById('password_confirmation').value;
                            var message = document.getElementById('message_about_password');
                            var submit_button = document.getElementById('submit_button');
                            if (password == password_confirmation) {
                                message.style.color = 'green';
                                message.innerHTML = 'matching';
                                submit_button.disabled = false;
                            } else {
                                message.style.color = 'red';
                                message.innerHTML = 'not matching';
                                submit_button.disabled = true;
                            }
                            if ((password == null && password_confirmation == null) || (password == "" && password_confirmation == "")) {
                                message.innerHTML = '';
                            }
                        }
                    </script>
                    <c:set var="admin" scope="session"/>
                    <c:if test="${admin == 'admin'}">
                        <input type="hidden" name="admin">
                    </c:if>
                    <input type="hidden" name="registration">
                    <div class="p-t-15">
                        <button class="btn btn--radius-2 btn--blue" type="submit" id="submit_button">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<!-- Jquery JS-->
<script src="resources/regform/vendor/jquery/jquery.min.js"></script>
<!-- Vendor JS-->
<script src="resources/regform/vendor/select2/select2.min.js"></script>
<script src="resources/regform/vendor/datepicker/moment.min.js"></script>
<script src="resources/regform/vendor/datepicker/daterangepicker.js"></script>

<!-- Main JS-->
<script src="resources/regform/js/global.js"></script>

</body><!-- This templates was made by Colorlib (https://colorlib.com) -->

</html>
<!-- end document-->