<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html lang="en">
<head>
	<title>Gifter Login</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="resources/loginform/images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="resources/loginform/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="resources/loginform/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="resources/loginform/fonts/iconic/css/material-design-iconic-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="resources/loginform/vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="resources/loginform/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="resources/loginform/vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="resources/loginform/vendor/select2/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="resources/loginform/vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="resources/loginform/css/util.css">
	<link rel="stylesheet" type="text/css" href="resources/loginform/css/main.css">
<!--===============================================================================================-->

	<c:if test="${login != null}">
		<script>window.location.href = 'profile'</script>
	</c:if>
</head>
<body>
	
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<form class="login100-form validate-form" action="LoginVerification" method="post">
					<span class="login100-form-title p-b-15">
						Welcome
					</span>
					<span class="login100-form-title p-b-29">
						<i class="zmdi zmdi-google-glass"></i>
					</span>

					<div class="wrap-input100 validate-input" data-validate = "Valid email is: a@b.c | Valid PN is: +1234567">
						<input class="input100" type="text" name="username" autocomplete="off">
						<span class="focus-input100" data-placeholder="Email or phone number"></span>
					</div>

					<div class="wrap-input100 validate-input" data-validate="Enter password">
						<span class="btn-show-pass">
							<i class="zmdi zmdi-eye"></i>
						</span>
						<input class="input100" type="password" name="password">
						<span class="focus-input100" data-placeholder="Password"></span>
					</div>

					<div class="container-login100-form-btn">
						<div class="wrap-login100-form-btn">
							<div class="login100-form-bgbtn"></div>
							<button class="login100-form-btn" type="submit">
								Login
							</button>
						</div>
					</div>

					<div class="text-center p-t-85">
						<span class="txt1">
							Don't have an account?
						</span>

						<a class="txt2" href="registration.jsp">
							Sign Up
						</a>
						<br>
						<span class="txt1">
							Forgot password?
						</span>
						<a class="txt2" href="reset-password.jsp">
							Reset
						</a>
					</div>
				</form>
			</div>
		</div>
	</div>
	

	<div id="dropDownSelect1"></div>
	
<!--===============================================================================================-->
	<script src="resources/loginform/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="resources/loginform/vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="resources/loginform/vendor/bootstrap/js/popper.js"></script>
	<script src="resources/loginform/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="resources/loginform/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="resources/loginform/vendor/daterangepicker/moment.min.js"></script>
	<script src="resources/loginform/vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="resources/loginform/vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="resources/loginform/js/main.js"></script>

</body>
</html>